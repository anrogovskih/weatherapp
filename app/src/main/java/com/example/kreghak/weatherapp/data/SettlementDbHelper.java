package com.example.kreghak.weatherapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SettlementDbHelper extends SQLiteOpenHelper {
    public static final String LOG_TAG = SettlementDbHelper.class.getSimpleName();

    /** Name of the database file */
    private static final String DATABASE_NAME = "settlements.db";

    private static final int DATABASE_VERSION = 1;

    public SettlementDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(LOG_TAG, "onCreate");
        // Create a String that contains the SQL statement to create the settlements table
        String SQL_CREATE_SETTLEMENTS_TABLE =  "CREATE TABLE " + SettlementContract.SettlementEntry.TABLE_NAME + " ("
                + SettlementContract.SettlementEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_NAME + " TEXT NOT NULL, "
                + SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_ID + " INTEGER NOT NULL, "
                + SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_TEMPERATURE + " DOUBLE, "
                + SettlementContract.SettlementEntry.COLUMN_UPDATE_TIME_STAMP + " LONG NOT NULL);";

        // Execute the SQL statement
        db.execSQL(SQL_CREATE_SETTLEMENTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
}
