package com.example.kreghak.weatherapp.net;

import android.app.Activity;
import android.content.AsyncTaskLoader;
import android.content.Context;

import java.lang.ref.WeakReference;

public class ForecastLoader extends AsyncTaskLoader<HttpTask> {
    private final HttpTask httpTask;
    private WeakReference<Activity> activityWeakReference;

    public ForecastLoader(Activity activity, HttpTask httpTask) {
        super(activity);
        this.httpTask = httpTask;
        activityWeakReference = new WeakReference<>(activity);
    }

    @Override
    public HttpTask loadInBackground() {
        // Perform the HTTP request for earthquake data and process the response.
        // Extract relevant fields from the JSON response
        return OkHttpLoader.getInstance(activityWeakReference.get()).fetchWeatherForecastData(httpTask);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
    }
}