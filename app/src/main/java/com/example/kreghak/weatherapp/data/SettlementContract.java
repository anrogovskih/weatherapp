package com.example.kreghak.weatherapp.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class SettlementContract {
    public static final String CONTENT_AUTHORITY = "com.example.kreghak.weatherapp";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_SETTLEMENTS = "settlements";

    public static final String LABEL_TAG = "LABEL";

    // To prevent someone from accidentally instantiating the contract class
    private SettlementContract() {}

    /**
     * Inner class that defines constant values for the settlements database table.
     * Each entry in the table represents a single settlement.
     */
    public static final class SettlementEntry implements BaseColumns {

        /** Name of database table for settlements */
        public final static String TABLE_NAME = "settlements";

        /**
         * The MIME type of the {@link #CONTENT_URI} for a list of pets.
         */
        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SETTLEMENTS;

        /**
         * The MIME type of the {@link #CONTENT_URI} for a single pet.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SETTLEMENTS;


        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_SETTLEMENTS);

        /**
         * Unique ID number for the settlement (only for use in the database table).
         *
         * Type: INTEGER
         */
        public final static String _ID = BaseColumns._ID;

        /**
         * Name of the settlement.
         *
         * Type: TEXT
         */
        public final static String COLUMN_SETTLEMENT_NAME ="name";

        /**
         * API id of the settlement.
         *
         * Type: LONG
         */
        public final static String COLUMN_SETTLEMENT_ID = "sid";

        /**
         * Current temperature in the settlement in Kelvins.
         *
         * Type: LONG
         */
        public final static String COLUMN_SETTLEMENT_TEMPERATURE = "temperature";


        /**
         * Current temperature in the settlement in Kelvins.
         *
         * Type: LONG
         */
        public final static String COLUMN_UPDATE_TIME_STAMP = "update_time";
    }
}
