package com.example.kreghak.weatherapp.net;

import com.example.kreghak.weatherapp.data.WeatherForecast;
import com.example.kreghak.weatherapp.enums.ForecastQueryType;
import com.example.kreghak.weatherapp.enums.TaskOnSuccess;

import java.util.ArrayList;

public class HttpTask {
    private final ForecastQueryType queryType;
    private final String uri;
    private TaskOnSuccess taskOnSuccess = TaskOnSuccess.UPDATE_UI;
    private ArrayList<WeatherForecast> forecasts;
    private int responseCode;

    public HttpTask(ForecastQueryType queryType, String uri, TaskOnSuccess taskOnSuccess) {
        this.queryType = queryType;
        this.uri = uri;
        this.taskOnSuccess = taskOnSuccess;
    }

    public HttpTask(ForecastQueryType queryType, String uri) {
        this.queryType = queryType;
        this.uri = uri;
    }

    public TaskOnSuccess getTaskOnSuccess() {
        return taskOnSuccess;
    }

    public void setTaskOnSuccess(TaskOnSuccess taskOnSuccess) {
        this.taskOnSuccess = taskOnSuccess;
    }

    public ArrayList<WeatherForecast> getForecasts() {
        return forecasts;
    }

    public void setForecasts(ArrayList<WeatherForecast> forecasts) {
        this.forecasts = forecasts;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public ForecastQueryType getQueryType() {
        return queryType;
    }

    public String getUri() {
        return uri;
    }
}
