package com.example.kreghak.weatherapp;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseLongArray;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kreghak.weatherapp.adapters.SettlementCursorAdapter;
import com.example.kreghak.weatherapp.adapters.SettlementsAdapter;
import com.example.kreghak.weatherapp.data.WeatherForecast;
import com.example.kreghak.weatherapp.enums.ForecastQueryType;
import com.example.kreghak.weatherapp.net.ForecastLoader;
import com.example.kreghak.weatherapp.net.HttpTask;
import com.example.kreghak.weatherapp.net.QueryUtils;

import java.util.ArrayList;
import java.util.HashMap;

import static com.example.kreghak.weatherapp.data.SettlementContract.LABEL_TAG;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_ID;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_NAME;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_TEMPERATURE;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_UPDATE_TIME_STAMP;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.CONTENT_URI;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry._ID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private ProgressBar mProgressBar;
    private ViewGroup mWarningLayout;

    private SettlementCursorAdapter adapter;
    private long lastUpdateTime;

    private SharedPreferences sharedPref;

    private static final int CURSOR_LOADER = 0;
    private static final int HTTP_LOADER = 2;
    //data update interval, 10 minutes *30 sec
    private static final long MIN_UPDATE_INTERVAL = /*30000*/600000;
    //data update interval, one hour *1 min
    private static final long DATA_VALIDITY_INTERVAL = /*60000*/3600000;

    private static final String API_REQUEST_URL = "http://api.openweathermap.org/data/2.5/group";
    private static final String UPDATE_KEY = "UPDATE_KEY";
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressBar = findViewById(R.id.id_progress_bar);
        mWarningLayout = findViewById(R.id.id__warning);

        // Setup FAB to open ForecastActivity
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ForecastActivity.class);
                intent.putExtra(LABEL_TAG, "Add new settlement");
                startActivity(intent);
            }
        });

        TextView againButton = mWarningLayout.findViewById(R.id.id__warning_message_button);
        againButton.setOnClickListener(this);

        sharedPref = getPreferences(Context.MODE_PRIVATE);
        lastUpdateTime = sharedPref.getLong(UPDATE_KEY, 0);

        initializeAdapter();

        getLoaderManager().initLoader(CURSOR_LOADER, null, cursorLoaderCallback);
        getLoaderManager().initLoader(HTTP_LOADER, null, weatherForecastLoaderCallback);
        Log.i(LOG_TAG, "onCreate");
    }

    private void initializeAdapter() {
        ListView displayView = findViewById(R.id.list_view);
        // Find and set empty view on the ListView, so that it only shows when the list has 0 items.
        View emptyView = findViewById(R.id.empty_view);
        adapter = new SettlementCursorAdapter(getApplicationContext(), null);
        displayView.setAdapter(adapter);
        displayView.setEmptyView(emptyView);
        displayView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Uri uri = ContentUris.withAppendedId(CONTENT_URI, id);
                Intent intent = new Intent(MainActivity.this, ForecastActivity.class);
                intent.setData(uri);
                intent.putExtra(LABEL_TAG, "Edit settlement");
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(LOG_TAG, "onStart");
        possiblyLoadWeather();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (sharedPref != null){
            sharedPref.edit().putLong(UPDATE_KEY, lastUpdateTime).apply();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_catalog.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_catalog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Delete all entries" menu option
            case R.id.action_delete_all_entries:
                getContentResolver().delete(CONTENT_URI, null, null);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        possiblyLoadWeather();
    }

    private void setLoading(boolean isLoading){
        if (mProgressBar != null) mProgressBar.setVisibility(isLoading? View.VISIBLE : View.INVISIBLE);
    }

    private void possiblyLoadWeather(){
        Log.i(LOG_TAG, "possiblyLoadWeather: requiresUpdate = " + requiresUpdate());
        if (adapter != null && adapter.getCursor() != null && adapter.getCount() > 0 && requiresUpdate()){

            setLoading(true);
            ArrayList <Integer> ids = new ArrayList<>();
            Cursor data = adapter.getCursor();
            Log.i(LOG_TAG, "possiblyLoadWeather: data.getCount() = " + data.getCount());
            data.moveToFirst();
            int id = data.getInt(data.getColumnIndexOrThrow(COLUMN_SETTLEMENT_ID));
            ids.add(id);

            while (data.moveToNext()) {
                /*int*/ id = data.getInt(data.getColumnIndexOrThrow(COLUMN_SETTLEMENT_ID));
                ids.add(id);
            }

            if (!ids.isEmpty()) {
                Bundle args = new Bundle();
                args.putIntegerArrayList("ids", ids);

                getLoaderManager().restartLoader(HTTP_LOADER, args, weatherForecastLoaderCallback).forceLoad();
            }
            else {
                Log.e(LOG_TAG, "ids are empty");
                updateUI(null);
            }
        }
    }

    /**
     * @return true if weather data is outdated and must be reloaded
     */
    private boolean isDataExpired(){
        return (lastUpdateTime == 0 || System.currentTimeMillis() - DATA_VALIDITY_INTERVAL > lastUpdateTime);
    }

    /**
     * @return true if it's time to query new data from net
     */
    private boolean requiresUpdate(){
        return (lastUpdateTime == 0 || System.currentTimeMillis() - MIN_UPDATE_INTERVAL > lastUpdateTime);
    }

    private void updateUI(HttpTask httpTask){
        setLoading(false);
        updateTemperature(httpTask != null? httpTask.getForecasts() : null);
    }

    private void updateTemperature(ArrayList<WeatherForecast> weatherForecasts){
        int rowsSaved = 0;
        if (weatherForecasts != null && !weatherForecasts.isEmpty()) {
            for (WeatherForecast wf : weatherForecasts) {
                try {
                    // Defines an object to contain the updated values
                    ContentValues values = new ContentValues();
                    values.put(COLUMN_SETTLEMENT_TEMPERATURE, wf.getWeatherMain().getTemp());
                    values.put(COLUMN_UPDATE_TIME_STAMP, System.currentTimeMillis());

                    rowsSaved += getContentResolver().update(
                            CONTENT_URI,
                            values,
                            COLUMN_SETTLEMENT_ID + "=" + wf.getSettlement().getId(),
                            null
                    );
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
            if (rowsSaved == adapter.getCount()){
                lastUpdateTime = System.currentTimeMillis();
                if (mWarningLayout.isShown()) mWarningLayout.setVisibility(View.GONE);
            }
        }
        if (isDataExpired()){
            mWarningLayout.setVisibility(View.VISIBLE);
        }
        Log.i(LOG_TAG, "rowsSaved = " + rowsSaved);
    }

    private LoaderManager.LoaderCallbacks<Cursor> cursorLoaderCallback = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//            Log.i(LOG_TAG, "cursorLoaderCallback: onCreateLoader");

            String[] projection = {
                    _ID,
                    COLUMN_SETTLEMENT_NAME,
                    COLUMN_SETTLEMENT_ID,
                    COLUMN_SETTLEMENT_TEMPERATURE,
                    COLUMN_UPDATE_TIME_STAMP
            };

            switch (id){
                case CURSOR_LOADER:
                    return new CursorLoader(
                            MainActivity.this,
                            CONTENT_URI,
                            projection,
                            null,
                            null,
                            null);
                default:
                    return null;
            }
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
//            Log.i(LOG_TAG, "cursorLoaderCallback: onLoadFinished");

            adapter.swapCursor(data);
            possiblyLoadWeather();
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
//            Log.i(LOG_TAG, "cursorLoaderCallback: onLoaderReset");

            // This is called when the last Cursor provided to onLoadFinished()
            // above is about to be closed.  We need to make sure we are no
            // longer using it.
            if (adapter != null)adapter.swapCursor(null);
        }
    };

    private LoaderManager.LoaderCallbacks<HttpTask> weatherForecastLoaderCallback = new LoaderManager.LoaderCallbacks<HttpTask>() {
        @Override
        public Loader<HttpTask> onCreateLoader(int id, Bundle args) {

            ArrayList <Integer> settlementIds = null;
            if (args != null){
                settlementIds = args.getIntegerArrayList("ids");
            }
            Log.i(LOG_TAG, "weatherForecastLoaderCallback: onCreateLoader");

            Uri baseUri = Uri.parse(API_REQUEST_URL);
            Uri.Builder uriBuilder = baseUri.buildUpon();

            if (settlementIds != null){
                String ids = "";
                for (int settlementId: settlementIds) {
                    if (!ids.isEmpty()){
                        ids = ids.concat(",");
                    }
                    ids = ids.concat(String.valueOf(settlementId));
                }
                uriBuilder.appendQueryParameter("id", ids);
            }
            uriBuilder.appendQueryParameter("appid", getString(R.string.open_weather_map_api_key));

            return new ForecastLoader(MainActivity.this, new HttpTask(ForecastQueryType.FORECAST_FOR_MULTIPLE_SETTLEMENTS, uriBuilder.toString()));
        }

        @Override
        public void onLoadFinished(Loader<HttpTask> loader, HttpTask httpTask) {
            Log.i(LOG_TAG, "weatherForecastLoaderCallback: onLoadFinished");
            updateUI(httpTask);
        }

        @Override
        public void onLoaderReset(Loader loader) {
            Log.i(LOG_TAG, "weatherForecastLoaderCallback: onLoaderReset");
        }
    };
}
