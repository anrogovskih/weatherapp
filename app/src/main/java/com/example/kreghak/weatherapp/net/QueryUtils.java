package com.example.kreghak.weatherapp.net;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.kreghak.weatherapp.data.WeatherForecast;
import com.example.kreghak.weatherapp.data.pojo.Coordinates;
import com.example.kreghak.weatherapp.data.pojo.Settlement;
import com.example.kreghak.weatherapp.data.pojo.Weather;
import com.example.kreghak.weatherapp.data.pojo.WeatherMain;
import com.example.kreghak.weatherapp.data.pojo.Wind;
import com.example.kreghak.weatherapp.enums.ForecastQueryType;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Helper methods related to requesting and receiving data from net.
 */
public class QueryUtils {

    private static final String LOG_TAG = QueryUtils.class.getSimpleName();
    private static final SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    static {
        mSimpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    /**
     * Create a private constructor because no one should ever create a {@link QueryUtils} object.
     * This class is only meant to hold static variables and methods, which can be accessed
     * directly from the class name QueryUtils (and an object instance of QueryUtils is not needed).
     */
    private QueryUtils() {

    }

    /**
     * Return a {@link WeatherForecast} object that has been built up from
     * parsing a JSON response.
     */
    static WeatherForecast extractTodaysWeather(HttpTask task, String jsonResponse) {
        WeatherForecast forecast = new WeatherForecast();

        if (jsonResponse == null || jsonResponse.equals("")){
            return forecast;
        }

        try {
            JSONObject jsonObject = new JSONObject(jsonResponse);
            parseCode(task, jsonObject);
            forecast = parseTodaysWeatherJson(jsonObject);
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing JSON results", e);
        }

        // Return the list of weatherForecasts
        return forecast;
    }

    private static void parseCode(HttpTask task, JSONObject jsonObject) {
        if (jsonObject.has("cod") && !jsonObject.isNull("cod")){
            try {
                task.setResponseCode(jsonObject.getInt("cod"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static WeatherForecast parseTodaysWeatherJson(JSONObject jsonObject) throws JSONException{
        WeatherForecast forecast = new WeatherForecast();

        Gson gson = new Gson();
        Coordinates coordinates = gson.fromJson(jsonObject.getJSONObject("coord").toString(), Coordinates.class);
        Weather weather = gson.fromJson(jsonObject.getJSONArray("weather").get(0).toString(), Weather.class);
        WeatherMain weatherMain = gson.fromJson(jsonObject.getJSONObject("main").toString(), WeatherMain.class);
        Wind wind = gson.fromJson(jsonObject.getJSONObject("wind").toString(), Wind.class);

        String settlementName = jsonObject.has("name") && !jsonObject.isNull("name")?
                jsonObject.getString("name") : null;
        int settlementId = jsonObject.has("id") && !jsonObject.isNull("id")?
                jsonObject.getInt("id") : 0;
        Settlement settlement = new Settlement(settlementName, settlementId);

        forecast.setCoordinates(coordinates);
        forecast.setSettlement(settlement);
        forecast.setWeather(weather);
        forecast.setWeatherMain(weatherMain);
        forecast.setWind(wind);
        return forecast;
    }

    static void extractTodaysWeatherForMultipleSettlements(HttpTask task, String jsonResponse) {
        ArrayList<WeatherForecast> forecasts = new ArrayList<>();

        if (jsonResponse == null || jsonResponse.equals("")){
            task.setForecasts(forecasts);
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(jsonResponse);
            parseCode(task, jsonObject);
            JSONArray jsonArray = jsonObject.getJSONArray("list");
            for (int i = 0; i < jsonArray.length(); i++) {
                WeatherForecast forecast = parseTodaysWeatherJson(jsonArray.getJSONObject(i));
                forecasts.add(forecast);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing JSON results", e);
        }
        task.setForecasts(forecasts);
    }

    static void extractForecast(HttpTask task, String jsonResponse){
        ArrayList<WeatherForecast> forecasts = new ArrayList<>();
        if (jsonResponse == null || jsonResponse.equals("")){
            task.setForecasts(forecasts);
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(jsonResponse);
            parseCode(task, jsonObject);
            Gson gson = new Gson();

            Settlement settlement = gson.fromJson(jsonObject.getJSONObject("city").toString(), Settlement.class);
            Coordinates coordinates = gson.fromJson(jsonObject.getJSONObject("city").getJSONObject("coord").toString(), Coordinates.class);
            JSONArray jsonArray = jsonObject.getJSONArray("list");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonItem = jsonArray.getJSONObject(i);
                Weather weather = gson.fromJson(jsonItem.getJSONArray("weather").get(0).toString(), Weather.class);
                WeatherMain weatherMain = gson.fromJson(jsonItem.getJSONObject("main").toString(), WeatherMain.class);
                Wind wind = gson.fromJson(jsonItem.getJSONObject("wind").toString(), Wind.class);
                String dateString = jsonItem.has("dt_txt") && !jsonItem.isNull("dt_txt")?
                        jsonItem.getString("dt_txt") : null;
                long date = 0;
                try {
                    date = mSimpleDateFormat.parse(dateString).getTime();
                }
                catch (ParseException e){
                    e.printStackTrace();
                }

                WeatherForecast forecast = new WeatherForecast();
                forecast.setCoordinates(coordinates);
                forecast.setSettlement(settlement);
                forecast.setWeather(weather);
                forecast.setWeatherMain(weatherMain);
                forecast.setWind(wind);
                forecast.setDateMillies(date);
                forecasts.add(forecast);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing JSON results", e);
        }
        task.setForecasts(forecasts);
    }

    public static boolean isConnected(Activity activity){
        if (activity != null) {
            ConnectivityManager cm =
                    (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                return netInfo != null && netInfo.isConnectedOrConnecting();
            }
        }
        return true;
    }
}
