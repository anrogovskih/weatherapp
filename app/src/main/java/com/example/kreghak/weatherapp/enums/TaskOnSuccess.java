package com.example.kreghak.weatherapp.enums;

public enum TaskOnSuccess {
    SAVE_AND_QUIT(1),

    UPDATE_UI(0);

    final int value;

    TaskOnSuccess(int value) {
        this.value = value;
    }

    public static TaskOnSuccess byInt(int value){
        switch (value){
            case 1:
                return SAVE_AND_QUIT;
            default:
                return UPDATE_UI;
        }
    }

    public int getValue() {
        return value;
    }
}
