package com.example.kreghak.weatherapp.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.kreghak.weatherapp.R;
import com.example.kreghak.weatherapp.data.WeatherForecast;
import com.example.kreghak.weatherapp.data.pojo.Weather;
import com.example.kreghak.weatherapp.data.pojo.WeatherMain;
import com.example.kreghak.weatherapp.data.pojo.Wind;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class OneDayForecastViewHolder extends RecyclerView.ViewHolder {
    public TextView dateTV;
    public TextView temperatureTV;
    public TextView humidityTV;
    public TextView windSpeedTV;
    public TextView descriptionTV;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM HH:mm", Locale.getDefault());

    public OneDayForecastViewHolder(View itemView) {
        super(itemView);
        dateTV = itemView.findViewById(R.id.id__date);
        temperatureTV = itemView.findViewById(R.id.id__temp_value);
        humidityTV = itemView.findViewById(R.id.id__humidity_value);
        windSpeedTV = itemView.findViewById(R.id.id__wind_speed_value);
        descriptionTV = itemView.findViewById(R.id.id__description_value);
    }

    public void bindItem(WeatherForecast forecast, boolean hasDate){
        if (forecast == null) return;
        Weather weather = forecast.getWeather();
        WeatherMain weatherMain = forecast.getWeatherMain();
        Wind wind = forecast.getWind();

        //TODO: set to gone if null
        if (weather != null){
            descriptionTV.setText(weather.getDescription());
        }
        if (weatherMain != null){
            temperatureTV.setText(getTemperatureString(weatherMain.getTemp()));
            humidityTV.setText(getHumidityString(weatherMain.getHumidity()));
        }

        if (wind != null){
            windSpeedTV.setText(getWindString(wind.getSpeed()));
        }

        if (hasDate && forecast.getDateMillies() > 0){
            dateTV.setVisibility(View.VISIBLE);
            dateTV.setText(getDateString(forecast.getDateMillies()));
        }
        else dateTV.setVisibility(View.GONE);
    }

    /**
     * @param temp - temperature in Kelvins
     * @return temperature in Celsius
     */
    public static String getTemperatureString(double temp){
        int celsiusT = (int)(temp - 273.15);
        if (celsiusT > 0){
            return "+" + celsiusT + " C";
        }
        else return celsiusT + " C";
    }

    private String getHumidityString(int humidity){
        return  humidity + "  %";
    }

    private String getPressureString(double pressure){
        return pressure + " hpa";
    }

    private String getWindString(double speed){
        return speed + "  m/s";
    }

    private String getDateString(long timeStamp){
        return dateFormat.format(new Date(timeStamp));
    }
}
