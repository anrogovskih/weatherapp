package com.example.kreghak.weatherapp.enums;

public enum ForecastQueryType {
    TODAY_S_WEATHER(0),
    FORECAST_FOR_SINGLE_SETTLEMENT(1),
    FORECAST_FOR_MULTIPLE_SETTLEMENTS(2),

    UNDEFINED(-1);

    final int value;

    ForecastQueryType(int value) {
        this.value = value;
    }

    public static ForecastQueryType byInt(int value){
        switch (value){
            case 0:
                return TODAY_S_WEATHER;
            case 1:
                return FORECAST_FOR_SINGLE_SETTLEMENT;
            case 2:
                return FORECAST_FOR_MULTIPLE_SETTLEMENTS;
                default:
                    return UNDEFINED;
        }
    }

    public int getValue() {
        return value;
    }
}
