package com.example.kreghak.weatherapp.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import static com.example.kreghak.weatherapp.data.SettlementContract.CONTENT_AUTHORITY;
import static com.example.kreghak.weatherapp.data.SettlementContract.PATH_SETTLEMENTS;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_ID;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_NAME;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_TEMPERATURE;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_UPDATE_TIME_STAMP;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.CONTENT_ITEM_TYPE;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.CONTENT_LIST_TYPE;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.TABLE_NAME;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry._ID;

public class SettlementProvider extends ContentProvider {
    private static final int SETTLEMENTS = 100;
    private static final int SETTLEMENT_ID = 101;


    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_SETTLEMENTS, SETTLEMENTS);
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_SETTLEMENTS + "/#", SETTLEMENT_ID);
    }

    /** Database helper that will provide us access to the database */
    private SettlementDbHelper mDbHelper;

    /**
     * Initialize the provider and the database helper object.
     */
    @Override
    public boolean onCreate() {
        mDbHelper = new SettlementDbHelper(getContext());
        return true;
    }

    /**
     * Perform the query for the given URI. Use the given projection, selection, selection arguments, and sort order.
     */
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        // Get readable database
        SQLiteDatabase database = mDbHelper.getReadableDatabase();

        // This cursor will hold the result of the query
        Cursor cursor;

        // Figure out if the URI matcher can match the URI to a specific code
        int match = sUriMatcher.match(uri);
        switch (match) {
            case SETTLEMENTS:
                // For the SETTLEMENTS code, query the settlements table directly with the given
                // projection, selection, selection arguments, and sort order. The cursor
                // could contain multiple rows of the settlements table.
                cursor = database.query(SettlementContract.SettlementEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case SETTLEMENT_ID:
                // For the SETTLEMENT_ID code, extract out the ID from the URI.
                // For an example URI such as "content://com.example.android.kreghak.weatherapp/3",
                // the selection will be "_id=?" and the selection argument will be a
                // String array containing the actual ID of 3 in this case.
                //
                // For every "?" in the selection, we need to have an element in the selection
                // arguments that will fill in the "?". Since we have 1 question mark in the
                // selection, we have 1 String in the selection arguments' String array.
                selection = _ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };

                // This will perform a query on the settlements table where the _id equals 3 to return a
                // Cursor containing that row of the table.
                cursor = database.query(TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }
        Context context = getContext();
        if (context != null) cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    /**
     * Insert new data into the provider with the given ContentValues.
     */
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues contentValues) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case SETTLEMENTS:
                return insertSettlement(uri, contentValues);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }

    /**
     * Insert a settlement into the database with the given content values. Return the new content URI
     * for that specific row in the database.
     */
    private Uri insertSettlement(Uri uri, ContentValues values) {
        checkContentValidityInsert(values);

        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        long id = database.insert(TABLE_NAME, null, values);
        // Once we know the ID of the new row in the table,
        // return the new URI with the ID appended to the end of it
        Uri itemUri = ContentUris.withAppendedId(uri, id);
        Context context = getContext();
        if (context != null)getContext().getContentResolver().notifyChange(itemUri, null);
        return itemUri;
    }

    private void checkContentValidityInsert(ContentValues values){
        checkName(values);
        checkId(values);
    }

    private void checkContentValidityUpdate(ContentValues values){
        if (values.containsKey(COLUMN_SETTLEMENT_NAME)) {
            checkName(values);
        }
        if (values.containsKey(COLUMN_SETTLEMENT_ID)){
            checkId(values);
        }
        if (values.containsKey(COLUMN_SETTLEMENT_TEMPERATURE)){
            checkTemperature(values);
        }
        if (values.containsKey(COLUMN_UPDATE_TIME_STAMP)){
            checkTimeStamp(values);
        }
    }

    private void checkName(ContentValues values){
        String name = values.getAsString(COLUMN_SETTLEMENT_NAME);
        if (name == null) {
            throw new IllegalArgumentException("Settlement requires a name");
        }
    }

    private void checkId(ContentValues values){
        Integer id = values.getAsInteger(COLUMN_SETTLEMENT_ID);
        if (id == null) {
            throw new IllegalArgumentException("Settlement requires an id");
        }
    }

    private void checkTemperature(ContentValues values){
        Double temp = values.getAsDouble(COLUMN_SETTLEMENT_TEMPERATURE);
        if (temp != null && temp < 0) {
            throw new IllegalArgumentException("Temperature in Kelvins can not be negative");
        }
    }

    private void checkTimeStamp(ContentValues values){
        Long timeStamp = values.getAsLong(COLUMN_UPDATE_TIME_STAMP);
        if (timeStamp != null && timeStamp <= 0) {
            throw new IllegalArgumentException("Time stamp can not be negative or zero");
        }
    }


    /**
     * Updates the data at the given selection and selection arguments, with the new ContentValues.
     */
    @Override
    public int update(@NonNull Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        checkContentValidityUpdate(contentValues);
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case SETTLEMENTS:
                return updateSettlement(uri, contentValues, selection, selectionArgs);
            case SETTLEMENT_ID:
                // For the SETTLEMENT_ID code, extract out the ID from the URI,
                // so we know which row to update. Selection will be "_id=?" and selection
                // arguments will be a String array containing the actual ID.
                selection = _ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                return updateSettlement(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }

    /**
     * Update settlements in the database with the given content values. Apply the changes to the rows
     * specified in the selection and selection arguments (which could be 0 or 1 or more settlements).
     * Return the number of rows that were successfully updated.
     */
    private int updateSettlement(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (values.size() == 0) return 0;
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        int rowsUpdated = database.update(TABLE_NAME, values, selection, selectionArgs);
        if (rowsUpdated > 0){
            Context context = getContext();
            if (context != null)getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    /**
     * Delete the data at the given selection and selection arguments.
     */
    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Context context = getContext();
        switch (match) {
            case SETTLEMENTS:
                int rowsDeleted = database.delete(TABLE_NAME, selection, selectionArgs);
                if (rowsDeleted != 0) {
                    if (context != null) context.getContentResolver().notifyChange(uri, null);
                }
                return rowsDeleted;
            case SETTLEMENT_ID:
                // For the SETTLEMENT_ID code, extract out the ID from the URI,
                // so we know which row to delete. Selection will be "_id=?" and selection
                // arguments will be a String array containing the actual ID.
                selection = _ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                rowsDeleted = database.delete(TABLE_NAME, selection, selectionArgs);
                if (rowsDeleted != 0) {
                    if (context != null) context.getContentResolver().notifyChange(uri, null);
                }
                return rowsDeleted;
            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }
    }

    /**
     * Returns the MIME type of data for the content URI.
     */
    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case SETTLEMENTS:
                return CONTENT_LIST_TYPE;
            case SETTLEMENT_ID:
                return CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }
}
