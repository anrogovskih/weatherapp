package com.example.kreghak.weatherapp.data.pojo;

public class Wind {
    private double speed;
    private double deg;

    public double getSpeed() {
        return speed;
    }

    public double getDeg() {
        return deg;
    }
}
