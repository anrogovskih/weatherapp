package com.example.kreghak.weatherapp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.kreghak.weatherapp.R;
import com.example.kreghak.weatherapp.data.WeatherForecast;
import com.example.kreghak.weatherapp.viewholders.OneDayForecastViewHolder;

import java.util.ArrayList;

public class ForecastAdapter extends RecyclerView.Adapter<OneDayForecastViewHolder> {
    private ArrayList<WeatherForecast> mDataSet;

    public ForecastAdapter(ArrayList<WeatherForecast> mDataSet) {
        this.mDataSet = mDataSet;
    }

    @NonNull
    @Override
    public OneDayForecastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OneDayForecastViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.one_day_weather_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OneDayForecastViewHolder holder, int position) {
        if (mDataSet != null) {
            WeatherForecast item = mDataSet.get(position);
            holder.bindItem(item, mDataSet.size() > 1);
        }
    }

    @Override
    public int getItemCount() {
        if (mDataSet != null){
            return mDataSet.size();
        }
        return 0;
    }

    public void clear(){
        if (mDataSet != null){
            mDataSet.clear();
            mDataSet = null;
            notifyDataSetChanged();
        }
    }
}
