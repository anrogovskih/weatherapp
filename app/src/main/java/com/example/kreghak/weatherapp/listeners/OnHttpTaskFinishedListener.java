package com.example.kreghak.weatherapp.listeners;

import com.example.kreghak.weatherapp.net.HttpTask;

public interface OnHttpTaskFinishedListener {
    void onHttpTaskFinished(HttpTask task);
}
