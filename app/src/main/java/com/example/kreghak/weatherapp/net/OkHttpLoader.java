package com.example.kreghak.weatherapp.net;

import android.app.Activity;

import com.example.kreghak.weatherapp.data.WeatherForecast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.example.kreghak.weatherapp.net.QueryUtils.extractForecast;
import static com.example.kreghak.weatherapp.net.QueryUtils.extractTodaysWeather;
import static com.example.kreghak.weatherapp.net.QueryUtils.extractTodaysWeatherForMultipleSettlements;

public class OkHttpLoader {
    private final OkHttpClient client;
    private static OkHttpLoader instance;

    private OkHttpLoader(Activity activity){
        int cacheSize = 10 * 1024; // 1 MiB
        Cache cache = new Cache(new File(activity.getApplication().getCacheDir(),"weatherCache"), cacheSize);
        client = new OkHttpClient.Builder().cache(cache).build();
    }

    public static OkHttpLoader getInstance(Activity activity){
        if (instance == null) instance = new OkHttpLoader(activity);
        return instance;
    }

    public HttpTask fetchWeatherForecastData(HttpTask httpTask) {
        httpTask.setResponseCode(200);
        Request request = new Request.Builder()
                .url(httpTask.getUri())
                .cacheControl(new CacheControl.Builder().maxStale(10, TimeUnit.MINUTES).build())
                .build();

        try {
            Response response = client.newCall(request).execute();
            ResponseBody body = response.body();
            if(body != null) {
                String jsonResponse = body.string();

                switch (httpTask.getQueryType()){
                    case TODAY_S_WEATHER:
                        ArrayList<WeatherForecast> forecasts = new ArrayList<>();
                        WeatherForecast weatherForecast = extractTodaysWeather(httpTask, jsonResponse);
                        if (weatherForecast != null) {
                            forecasts.add(weatherForecast);
                        }
                        httpTask.setForecasts(forecasts);
                        break;
                    case FORECAST_FOR_SINGLE_SETTLEMENT:
                        extractForecast(httpTask, jsonResponse);
                        break;
                    case FORECAST_FOR_MULTIPLE_SETTLEMENTS:
                        extractTodaysWeatherForMultipleSettlements(httpTask, jsonResponse);
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            httpTask.setResponseCode(408);
        }

        return httpTask;
    }
}
