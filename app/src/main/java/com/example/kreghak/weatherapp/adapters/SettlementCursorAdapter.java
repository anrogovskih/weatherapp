package com.example.kreghak.weatherapp.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.kreghak.weatherapp.R;
import com.example.kreghak.weatherapp.viewholders.OneDayForecastViewHolder;

import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_NAME;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_TEMPERATURE;

public class SettlementCursorAdapter extends CursorAdapter {

    public SettlementCursorAdapter(Context context, Cursor c) {
        super(context, c, 0 /* flags */);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.settlement_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView name = view.findViewById(R.id.id__name);
        TextView tempTV = view.findViewById(R.id.id__temperature);

        String nameString = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_SETTLEMENT_NAME));
        double temperature = cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_SETTLEMENT_TEMPERATURE));

        name.setText(nameString);
        tempTV.setText(OneDayForecastViewHolder.getTemperatureString(temperature));
        int celsiusT = (int)(temperature - 273.15);
        if (celsiusT > 0){
            tempTV.setBackgroundResource(R.drawable.hot_temp_bg);
        }
        else {
            tempTV.setBackgroundResource(R.drawable.cold_temp_bg);
        }
    }
}
