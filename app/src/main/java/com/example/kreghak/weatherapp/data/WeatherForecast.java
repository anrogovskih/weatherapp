package com.example.kreghak.weatherapp.data;

import com.example.kreghak.weatherapp.data.pojo.Coordinates;
import com.example.kreghak.weatherapp.data.pojo.Settlement;
import com.example.kreghak.weatherapp.data.pojo.Weather;
import com.example.kreghak.weatherapp.data.pojo.WeatherMain;
import com.example.kreghak.weatherapp.data.pojo.Wind;

public class WeatherForecast {
    private Settlement settlement;
    private Coordinates coordinates;
    private Weather weather;
    private WeatherMain weatherMain;
    private Wind wind;
    private long dateMillies;

    public Settlement getSettlement() {
        return settlement;
    }

    public void setSettlement(Settlement settlement) {
        this.settlement = settlement;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public WeatherMain getWeatherMain() {
        return weatherMain;
    }

    public void setWeatherMain(WeatherMain weatherMain) {
        this.weatherMain = weatherMain;
    }

    public long getDateMillies() {
        return dateMillies;
    }

    public void setDateMillies(long dateMillies) {
        this.dateMillies = dateMillies;
    }
}
