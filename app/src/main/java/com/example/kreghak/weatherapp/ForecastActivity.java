package com.example.kreghak.weatherapp;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kreghak.weatherapp.adapters.ForecastAdapter;
import com.example.kreghak.weatherapp.data.WeatherForecast;
import com.example.kreghak.weatherapp.data.pojo.Settlement;
import com.example.kreghak.weatherapp.enums.ForecastQueryType;
import com.example.kreghak.weatherapp.enums.TaskOnSuccess;
import com.example.kreghak.weatherapp.net.ForecastLoader;
import com.example.kreghak.weatherapp.net.HttpTask;
import com.example.kreghak.weatherapp.net.QueryUtils;

import java.util.ArrayList;
import java.util.Date;

import static com.example.kreghak.weatherapp.data.SettlementContract.LABEL_TAG;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_ID;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_NAME;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_SETTLEMENT_TEMPERATURE;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.COLUMN_UPDATE_TIME_STAMP;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry.CONTENT_URI;
import static com.example.kreghak.weatherapp.data.SettlementContract.SettlementEntry._ID;

public class ForecastActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {

    /** EditText field to enter the settlement's name */
    private EditText mNameEditText;
    private TextView mNameTextView;
    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private TextView mForecastHintTextView;

    private ForecastAdapter adapter;

    private Uri currentUri;

    private Settlement settlement;
    private double currentTemperature;

    private static final int CURSOR_LOADER = 1;
    private static final int HTTP_LOADER = 2;
    private static final String BUNDLE_KEY_TYPE = "type";
    private static final String BUNDLE_KEY_TASK = "task";
    private static final String BUNDLE_KEY_NAME = "name";

    private static final String LOG_TAG = ForecastActivity.class.getSimpleName();
    private static final String API_REQUEST_URL = "http://api.openweathermap.org/data/2.5";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);

        // Find all relevant views that we will need to read user input from
        mNameEditText = findViewById(R.id.edit_name);
        mNameTextView = findViewById(R.id.id_name);
        mProgressBar = findViewById(R.id.id_progress_bar);
        mRecyclerView = findViewById(R.id.id__recycler_view);
        mForecastHintTextView = findViewById(R.id.id__forecast_hint);
        View showForecastButton = findViewById(R.id.id__show_forecast_button);
        View showSeveralDaysForecastButton = findViewById(R.id.id__show_5_days_forecast_button);

        Intent intent = getIntent();
        currentUri = intent.getData();
        setTitle(intent.getStringExtra(LABEL_TAG));
        showForecastButton.setOnClickListener(this);
        showSeveralDaysForecastButton.setOnClickListener(this);

        getLoaderManager().initLoader(HTTP_LOADER, null, weatherForecastLoaderCallback);

        if (currentUri != null){
            mNameEditText.setVisibility(View.GONE);
            mNameTextView.setVisibility(View.VISIBLE);
            showForecastButton.setVisibility(View.GONE);
            getLoaderManager().initLoader(CURSOR_LOADER, null, cursorLoaderCallback);
        }
        else {
            mNameEditText.addTextChangedListener(this);
            mNameTextView.setVisibility(View.GONE);
        }
        invalidateOptionsMenu();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(LOG_TAG, "onStart");

    }

    /**
     * Get user input from editor and save new settlement into database.
     *
     * @return true if saving was successful
     */
    private boolean saveSettlement(boolean isFromOnClick) {
        if (settlement == null) {
            //if user entered incorrect name
            if (adapter != null) {
                Toast.makeText(this, "Error with saving settlement: settlement was not recognized", Toast.LENGTH_SHORT).show();
            }
            //if input field is empty
            else if (mNameEditText != null && mNameEditText.isShown() && TextUtils.isEmpty(mNameEditText.getText())){
                showErrorSettlementIsEmpty();
            }
            //if user did not click "show weather" and wants just add a new settlement
            else if (isFromOnClick){
                loadForecast(ForecastQueryType.TODAY_S_WEATHER, TaskOnSuccess.SAVE_AND_QUIT);
            }
            return false;
        }

        // Defines a new Uri object that receives the result of the insertion
        Uri mNewUri;

        // Create a ContentValues object where column names are the keys,
        // and settlement attributes from the editor are the values.
        ContentValues values = new ContentValues();
        values.put(COLUMN_SETTLEMENT_NAME, settlement.getName());
        values.put(COLUMN_SETTLEMENT_ID, settlement.getId());
        values.put(COLUMN_SETTLEMENT_TEMPERATURE, currentTemperature);
        values.put(COLUMN_UPDATE_TIME_STAMP, System.currentTimeMillis());

        if (currentUri == null) {
            // Insert a new row for settlement in the database, returning the ID of that new row.
            mNewUri = getContentResolver().insert(
                    CONTENT_URI,   // the user dictionary content URI
                    values                          // the values to insert
            );
            long newRowId = -1;
            if (mNewUri != null) {
                newRowId = ContentUris.parseId(mNewUri);
            }

            // Show a toast message depending on whether or not the insertion was successful
            if (newRowId == -1) {
                // If the row ID is -1, then there was an error with insertion.
                Toast.makeText(this, "Error with saving settlement", Toast.LENGTH_SHORT).show();
                return false;
            } else {
                // Otherwise, the insertion was successful and we can display a toast with the row ID.
                Toast.makeText(this, "Settlement saved with row id: " + newRowId, Toast.LENGTH_SHORT).show();
                return true;
            }
        }
        else return false;
    }

    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the settlement.
                deleteSettlement(dialog);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the settlement.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Perform the deletion of the settlement in the database.
     */
    private void deleteSettlement(DialogInterface dialog) {
        if (currentUri == null){
            Toast.makeText(this, getString(R.string.editor_delete_settlement_failed), Toast.LENGTH_SHORT).show();
            return;
        }
        int result = getContentResolver().delete(currentUri, null, null);
        if (result > 0){
            getLoaderManager().destroyLoader(CURSOR_LOADER);
            Toast.makeText(this, getString(R.string.editor_delete_settlement_successful), Toast.LENGTH_SHORT).show();
            if (dialog != null) {
                dialog.dismiss();
            }
            finish();
        }
        else Toast.makeText(this, getString(R.string.editor_delete_settlement_failed), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                // Save settlement to database
                if (saveSettlement(true)) {
                    // Exit activity
                    finish();
                }
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(ForecastActivity.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // If this is a new settlement, hide the "Delete" menu item.
        if (currentUri == null) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        else {
            MenuItem menuItem = menu.findItem(R.id.action_save);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.id__show_forecast_button:
                loadForecast(ForecastQueryType.TODAY_S_WEATHER, TaskOnSuccess.UPDATE_UI);
                break;
            case R.id.id__show_5_days_forecast_button:
                loadForecast(ForecastQueryType.FORECAST_FOR_SINGLE_SETTLEMENT, TaskOnSuccess.UPDATE_UI);
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (adapter != null){
            adapter.clear();
            adapter = null;
        }
    }

    private void setLoading(boolean isLoading){
        if (mProgressBar != null) mProgressBar.setVisibility(isLoading? View.VISIBLE : View.INVISIBLE);
    }

    private void loadForecast(ForecastQueryType type, TaskOnSuccess taskOnSuccess){
        String nameString = null;
        if (mNameEditText != null && mNameEditText.isShown()) {
            nameString = mNameEditText.getText().toString().trim();
        } else if (settlement != null) {
            nameString = settlement.getName();
        }

        if (!TextUtils.isEmpty(nameString) && type != null){
            setLoading(true);

            Bundle bundle = new Bundle();
            bundle.putString(BUNDLE_KEY_NAME, nameString);
            bundle.putInt(BUNDLE_KEY_TYPE, type.getValue());
            bundle.putInt(BUNDLE_KEY_TASK, taskOnSuccess.getValue());

            getLoaderManager().restartLoader(HTTP_LOADER, bundle, weatherForecastLoaderCallback).forceLoad();
        }
        else if (TextUtils.isEmpty(nameString)){
            showErrorSettlementIsEmpty();
        }
    }

    private void updateUI(ArrayList<WeatherForecast> weatherForecast){
        if (weatherForecast != null && !weatherForecast.isEmpty()) {
            if (weatherForecast.size() == 1) {
                extractSettlement(weatherForecast);
            }
            else mForecastHintTextView.setVisibility(View.VISIBLE);
            adapter = new ForecastAdapter(weatherForecast);
            mRecyclerView.setAdapter(adapter);
        }
        else {
            showErrorUndefined();
        }
    }

    private void extractSettlement(ArrayList<WeatherForecast> weatherForecast){
        if (weatherForecast != null && !weatherForecast.isEmpty()) {
            try {
                settlement = weatherForecast.get(0).getSettlement();
                currentTemperature = weatherForecast.get(0).getWeatherMain().getTemp();
            }
            catch (NullPointerException e){
                e.printStackTrace();
            }
        }
    }

    private void showErrorUnknownSettlement(){
        Toast.makeText(this, "Sorry, we don't know this city", Toast.LENGTH_SHORT).show();
    }

    private void showErrorSettlementIsEmpty(){
        Toast.makeText(this, "Enter settlement name", Toast.LENGTH_SHORT).show();
    }

    private void showErrorUndefined() {
        Toast.makeText(this, "Something went wrong, try again later", Toast.LENGTH_SHORT).show();
    }

    private void showErrorNoConnection(){
        Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
    }

        private LoaderManager.LoaderCallbacks<HttpTask> weatherForecastLoaderCallback = new LoaderManager.LoaderCallbacks<HttpTask>() {
        @Override
        public Loader<HttpTask> onCreateLoader(int id, Bundle args) {

            String name = "";
            ForecastQueryType type = ForecastQueryType.UNDEFINED;
            TaskOnSuccess taskOnSuccess = TaskOnSuccess.UPDATE_UI;
            if (args != null){
                name = args.getString(BUNDLE_KEY_NAME);
                type = ForecastQueryType.byInt(args.getInt(BUNDLE_KEY_TYPE));
                taskOnSuccess = TaskOnSuccess.byInt(args.getInt(BUNDLE_KEY_TASK));
            }
            Log.i(LOG_TAG, "onCreateLoader, name is " + name);

            Uri baseUri = Uri.parse(API_REQUEST_URL);
            Uri.Builder uriBuilder = baseUri.buildUpon();
            switch (type){
                case TODAY_S_WEATHER:
                    uriBuilder.appendPath("weather");
                    break;
                case FORECAST_FOR_SINGLE_SETTLEMENT:
                    uriBuilder.appendPath("forecast");
            }
            uriBuilder.appendQueryParameter("q", name);
            uriBuilder.appendQueryParameter("appid", getString(R.string.open_weather_map_api_key));

            return new ForecastLoader(ForecastActivity.this, new HttpTask(type, uriBuilder.toString(), taskOnSuccess));
        }

        @Override
        public void onLoadFinished(Loader<HttpTask> loader, HttpTask httpTask) {
            Log.i(LOG_TAG, "weatherForecastLoaderCallback: onLoadFinished");

            setLoading(false);
            if (httpTask != null){
                switch (httpTask.getResponseCode()){
                    case 200:
                        switch (httpTask.getTaskOnSuccess()){
                            case UPDATE_UI:
                                updateUI(httpTask.getForecasts());
                                break;
                            case SAVE_AND_QUIT:
                                extractSettlement(httpTask.getForecasts());
                                if (saveSettlement(false)){
                                    finish();
                                }
                                break;
                        }
                        break;
                    case 404:
                        showErrorUnknownSettlement();
                        break;
                        default:
                            if (QueryUtils.isConnected(ForecastActivity.this)){
                                showErrorUndefined();
                            }
                            else showErrorNoConnection();

                }
            }
        }

        @Override
        public void onLoaderReset(Loader loader) {
            Log.i(LOG_TAG, "weatherForecastLoaderCallback: onLoaderReset");
        }
    };

    private LoaderManager.LoaderCallbacks<Cursor> cursorLoaderCallback = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            // Define a projection that specifies which columns from the database
            // you will actually use after this query.
            String[] projection = {
                    _ID,
                    COLUMN_SETTLEMENT_NAME,
                    COLUMN_SETTLEMENT_ID,
            };

            return new CursorLoader(
                    ForecastActivity.this,
                    currentUri,
                    projection,
                    null,
                    null,
                    null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            if (data != null && data.moveToFirst()) {
                String name = data.getString(data.getColumnIndex(COLUMN_SETTLEMENT_NAME));
                int id = data.getInt(data.getColumnIndex(COLUMN_SETTLEMENT_ID));
                mNameTextView.setText(name);

                settlement = new Settlement(name, id);
                loadForecast(ForecastQueryType.TODAY_S_WEATHER, TaskOnSuccess.UPDATE_UI);
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mNameTextView.setText("");
        }
    };
}
