package com.example.kreghak.weatherapp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.kreghak.weatherapp.R;
import com.example.kreghak.weatherapp.data.WeatherForecast;
import com.example.kreghak.weatherapp.data.pojo.Settlement;
import com.example.kreghak.weatherapp.data.pojo.WeatherMain;
import com.example.kreghak.weatherapp.viewholders.OneDayForecastViewHolder;

import java.util.ArrayList;

public class SettlementsAdapter extends RecyclerView.Adapter<SettlementsAdapter.SettlementViewHolder> {

    private ArrayList<WeatherForecast> mDataSet;
    private View.OnClickListener mOnItemClickListener;

    public SettlementsAdapter(ArrayList<WeatherForecast> mDataSet, View.OnClickListener onClickListener) {
        this.mDataSet = mDataSet;
        mOnItemClickListener = onClickListener;
    }

    @NonNull
    @Override
    public SettlementViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SettlementViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.settlement_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SettlementViewHolder holder, int position) {
        WeatherForecast item = mDataSet.get(position);
        if (item != null) {
            WeatherMain weatherMain = item.getWeatherMain();
            if (weatherMain != null){
                holder.tempTV.setText(OneDayForecastViewHolder.getTemperatureString(weatherMain.getTemp()));
                int celsiusT = (int)(weatherMain.getTemp() - 273.15);
                if (celsiusT > 0){
                    holder.tempTV.setBackgroundResource(R.drawable.hot_temp_bg);
                }
                else {
                    holder.tempTV.setBackgroundResource(R.drawable.cold_temp_bg);
                }
            }
            Settlement settlement = item.getSettlement();
            if (settlement != null){
                holder.nameTV.setText(settlement.getName());
                holder.itemView.setTag(settlement.getId());
                holder.itemView.setOnClickListener(mOnItemClickListener);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mDataSet != null) return mDataSet.size();
        return 0;
    }

    public void clear(){
        mDataSet = null;
        notifyDataSetChanged();
    }

    class SettlementViewHolder extends RecyclerView.ViewHolder {
        TextView tempTV;
        TextView nameTV;
        SettlementViewHolder(View itemView) {
            super(itemView);
            nameTV = itemView.findViewById(R.id.id__name);
            tempTV = itemView.findViewById(R.id.id__temperature);

        }
    }
}
